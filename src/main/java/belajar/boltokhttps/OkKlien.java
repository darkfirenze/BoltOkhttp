package belajar.boltokhttps;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

/**
 * Created by Kucing Imut on 9/21/15.
 */




public class OkKlien {

    private static OkHttpClient okHttpClient;

    public OkKlien() {
    }



    public static OkHttpClient getOkHttpClient() {

        if (okHttpClient == null) {

            okHttpClient = new OkHttpClient();
            okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);
            okHttpClient.setReadTimeout(40, TimeUnit.SECONDS);
        }

        return okHttpClient;
    }

}
