package belajar.boltokhttps;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

import bolts.CancellationTokenSource;
import bolts.Continuation;
import bolts.Task;

public class MainActivity extends AppCompatActivity {


    private ProgressDialog progressDialog;
    private ListView listView;
    private Button tombol;
    private List<Berita> arrlistberita;

    private Request requestString;
    private String hasiljson = "";
    private OkHttpClient okHttpClient;
    private Deserials deserials;

    private CancellationTokenSource cancellationToken;
    private Call callOkhttp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_utama);

        deserials = new Deserials();

        listView = (ListView) findViewById(R.id.list_hasil);

        tombol = (Button) findViewById(R.id.tombol_ambildatabolt);
        tombol.setOnClickListener(listener);

        cancellationToken = new CancellationTokenSource();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        cancellationToken.cancel();

        try {
            if (callOkhttp != null) {
                callOkhttp.cancel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

//            ambilDataBoltOkhttp();
            ambilDataBoltOkhttpGson();

        }
    };


    //AMBIL DATA DENGAN BOLT DAN OKHTTP
    private void ambilDataBoltOkhttp() {

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("AMBIL DATA");
        progressDialog.setCancelable(false);
        progressDialog.show();


        final String urlAPI = "http://www.codepolitan.com/api/posts/latest/post/1";
        final RequestBuilds requestBuilds = new RequestBuilds();


        //jalankan di background
        Task.callInBackground(new Callable<String>() {

            @Override
            public String call() throws Exception {

                requestString = requestBuilds.buildRequestGET(urlAPI);
                okHttpClient = OkKlien.getOkHttpClient();

                callOkhttp = okHttpClient.newCall(requestString);
                Response response = callOkhttp.execute();

                String respons = "";
                if (response.isSuccessful()) {
                    respons = deserials.parseResponString(response.body().byteStream());
                } else {
                    respons = "";
                }

                return respons;
            }
        })

                .continueWith(new Continuation<String, List<Berita>>() {
                    @Override
                    public List<Berita> then(Task<String> task) throws Exception {

                        //lanjutkan dengan parse dengan gson
                        hasiljson = task.getResult();
                        List<Berita> arrayBerita = deserials.deserialJSONGson(hasiljson);
                        Log.w("JSON PARSE", "json " + hasiljson);

                        return arrayBerita;
                    }

                }, Task.BACKGROUND_EXECUTOR, cancellationToken.getToken())


                .continueWith(new Continuation<List<Berita>, Object>() {
                    @Override
                    public Object then(Task<List<Berita>> task) throws Exception {

                        arrlistberita = task.getResult();
                        progressDialog.dismiss();

                        //tampilkan ke listview dengan menjalankan di ui thread
                        if (arrlistberita != null) {

                            Log.w("HASIL LIST", "HASIL LIST " + arrlistberita.size());
                            listView.setAdapter(new AdapterList());
                        } else {
                            Toast.makeText(MainActivity.this, "GAGAL PARSE", Toast.LENGTH_SHORT).show();
                        }

                        return null;
                    }

                }, Task.UI_THREAD_EXECUTOR, cancellationToken.getToken());
    }









    //AMBIL DATA DENGAN BOLT DAN OKHTTP GSON DESERIALIZER
    private void ambilDataBoltOkhttpGson() {

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("AMBIL DATA");
        progressDialog.setCancelable(false);
        progressDialog.show();


//        final String urlAPI = "http://www.codepolitan.com/api/posts/latest/post/1";
//        final RequestBuilds requestBuilds = new RequestBuilds();


        //jalankan di background
        Task.callInBackground(new Callable<ArrayBerita>() {

            @Override
            public ArrayBerita call() throws Exception {

                String urlAPI = "http://www.codepolitan.com/api/posts/latest/post/1";
                RequestBuilds requestBuilds = new RequestBuilds();

                requestString = requestBuilds.buildRequestGET(urlAPI);

                callOkhttp = OkKlien.getOkHttpClient().newCall(requestString);
                Response response = callOkhttp.execute();

                DeserialGSON<ArrayBerita> deserialGSON = new DeserialGSON(ArrayBerita.class);

                //task bolt akan otomatis menjadi gagal dan akan return null jika ada error di dalamnya
                //dan akan menghasilkan exception
                return deserialGSON.deserialGSONJson(response.body().byteStream());
            }
        })

                .continueWith(new Continuation<ArrayBerita, Object>() {

                    @Override
                    public Object then(Task<ArrayBerita> task) throws Exception {

                        //jika task sebelumnya gagal maka handle nya didalam sini
                        if (task.isFaulted()) {
                            Log.w("TASK GAGAL","TASK SEBELUM NYA GAGAL PARSE");

                            Toast.makeText(MainActivity.this, "GAGAL PARSE", Toast.LENGTH_SHORT).show();
                        }
                        else {

                            ArrayBerita arrayBerita = task.getResult();
                            arrlistberita = arrayBerita.getResult();
                            listView.setAdapter(new AdapterList());
                            Log.w("HASIL LIST", "HASIL LIST " + arrlistberita.size());

                        }

                        progressDialog.dismiss();

//                        ArrayBerita arrayBerita = task.getResult();
//                        if (arrayBerita != null) {
//
//                            arrlistberita = task.getResult().getResult();
//                            listView.setAdapter(new AdapterList());
//                            Log.w("HASIL LIST", "HASIL LIST " + arrlistberita.size());
//
//                        } else {
//                            Toast.makeText(MainActivity.this, "GAGAL PARSE", Toast.LENGTH_SHORT).show();
//                        }

                        return null;
                    }
                }, Task.UI_THREAD_EXECUTOR, cancellationToken.getToken());
    }


    protected class AdapterList extends ArrayAdapter<Berita> {

        private LayoutInflater inflaters;

        public AdapterList() {
            super(MainActivity.this, R.layout.rowitems, arrlistberita);
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolders viewholder;

            if (convertView == null) {

                inflaters = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflaters.inflate(R.layout.rowitems, parent, false);

                viewholder = new ViewHolders(convertView);
                convertView.setTag(viewholder);
            } else {
                viewholder = (ViewHolders) convertView.getTag();
            }

            Berita berita = arrlistberita.get(position);
            viewholder.getTeksjudul().setText(berita.getTitle());


            return convertView;
        }
    }


}
