package belajar.boltokhttps;

import com.google.gson.Gson;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okio.BufferedSource;
import okio.Okio;

/**
 * Created by Kucing Imut on 9/21/15.
 */
public class Deserials {


    public Deserials() {
    }


    public String parseResponString(InputStream inputStream) {

        String jsons = "";
        try {
            BufferedSource bufferedSource = Okio.buffer(Okio.source(inputStream));
            jsons = bufferedSource.readUtf8();

            bufferedSource.close();

        } catch (Exception e) {
            e.printStackTrace();
            jsons = "";
        }

        return jsons;
    }


    //PARSE JSON GSON
    public List<Berita> deserialJSONGson(String jsons) {

        List<Berita> listberita = new ArrayList<>();

        try {

            Gson gson = new Gson();

            ArrayBerita arrayberita = gson.fromJson(jsons, ArrayBerita.class);
            listberita = arrayberita.getResult();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            listberita = null;
        }


        return listberita;
    }



}
