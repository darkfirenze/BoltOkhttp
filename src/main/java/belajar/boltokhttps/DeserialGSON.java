package belajar.boltokhttps;

import com.google.gson.Gson;

import java.io.InputStream;

import okio.BufferedSource;
import okio.Okio;

/**
 * Created by Kucing Imut on 9/21/15.
 */
public class DeserialGSON<T> {


    private final Gson gson;
    private final Class<T> clazz;

    public DeserialGSON(Class<T> tClass) {

        gson = new Gson();
        this.clazz = tClass;

    }


    public String parseResponString(InputStream inputStream) {

        String jsons = "";
        try {
            BufferedSource bufferedSource = Okio.buffer(Okio.source(inputStream));
            jsons = bufferedSource.readUtf8();

            bufferedSource.close();

        } catch (Exception e) {
            e.printStackTrace();
            jsons = "";
        }

        return jsons;
    }



    public T deserialGSONJson(InputStream inputStream) {

        String jsons = "";
        T kelasT;

        try {
            BufferedSource bufferedSource = Okio.buffer(Okio.source(inputStream));
            jsons = bufferedSource.readUtf8();

            bufferedSource.close();

        } catch (Exception e) {
            e.printStackTrace();
            jsons = "";
        }


        try {
            kelasT = gson.fromJson(jsons, clazz);
        } catch (Exception ex) {
            ex.printStackTrace();
            kelasT = null;
        }


        return kelasT;
    }

}
