package belajar.boltokhttps;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;

/**
 * Created by Gulajava Ministudio on 9/19/15.
 */
public class RequestBuilds {

    private MultipartBuilder multipartBuilder;
    private FormEncodingBuilder formEncodingBuilder;

    private Map<String, String> headersmap;
    private Map<String, String> bodystrings;
    private Map<String, String> bodyfiles;


    public RequestBuilds() {

        multipartBuilder = new MultipartBuilder();
        formEncodingBuilder = new FormEncodingBuilder();

        headersmap = new HashMap<>();
        bodystrings = new HashMap<>();
        bodyfiles = new HashMap<>();

    }


    public RequestBuilds addHeaders(String paramheader, String valheaders) {

        headersmap.put(paramheader, valheaders);

        return this;
    }

    public RequestBuilds addBodyStrings(String parambody, String valbody) {

        bodystrings.put(parambody, valbody);

        return this;
    }


    public RequestBuilds addBodyFiles(String parambody, String alamatfiles) {

        bodyfiles.put(parambody, alamatfiles);

        return this;
    }


    public void setHeadersmap(Map<String, String> headersmap) {
        this.headersmap = headersmap;
    }

    public void setBodystrings(Map<String, String> bodystrings) {
        this.bodystrings = bodystrings;
    }


    public void setBodyFilesMultiparts(Map<String, String> bodyfilesmultiparts) {

        this.bodyfiles = bodyfilesmultiparts;
    }




    /** REQUEST DENGAN METODE GET **/
    public Request buildRequestGET(String stringUrls) {

        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(stringUrls);

        headersmap.put("Content-Type", "application/x-www-form-urlencoded");

        int panjangheader = headersmap.size();

        if (panjangheader > 0) {

            for (String key : headersmap.keySet()) {

                String value = headersmap.get(key);
                requestBuilder.addHeader(key, value);
            }
        }

        return requestBuilder.build();
    }





    /** REQUEST DENGAN METODE POST **/
    public Request buildRequestPostForm(String urls) {

        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(urls);

        //tambahkan header
        int panjangheader = headersmap.size();
        if (panjangheader > 0) {

            for (String key : headersmap.keySet()) {

                String value = headersmap.get(key);
                requestBuilder.addHeader(key, value);
            }
        }


        //tambahkan header
        int panjangbodystring = bodystrings.size();
        if (panjangbodystring > 0) {

            for (String key : bodystrings.keySet()) {

                String value = bodystrings.get(key);
                formEncodingBuilder.add(key, value);
            }
        }

        //POST wajib ada http body nya
        RequestBody requestBody = formEncodingBuilder.build();
        requestBuilder.post(requestBody);

        return requestBuilder.build();
    }


    /**
     * MULTIPART REQUEST BIASA
     **/
    public Request buildMultipartRequest(String urls) {

        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(urls);


        //tambahkan header
        int panjangheader = headersmap.size();
        if (panjangheader > 0) {

            for (String key : headersmap.keySet()) {

                String value = headersmap.get(key);
                requestBuilder.addHeader(key, value);
            }
        }


        //tambahkan header
        int panjangbodystring = bodystrings.size();
        if (panjangbodystring > 0) {

            for (String key : bodystrings.keySet()) {

                String value = bodystrings.get(key);
                multipartBuilder.addFormDataPart(key, value);

            }
        }


        //masukkan file ke dalam body
        int panjangbodyfiles = bodyfiles.size();
        if (panjangbodyfiles > 0) {

            for (String key : bodyfiles.keySet()) {

                String alamatfiles = bodyfiles.get(key);

                try {
                    File value = new File(alamatfiles);
                    String name = value.getName();
                    String contentType = URLConnection.guessContentTypeFromName(name);

                    multipartBuilder.addFormDataPart(key, name,
                            RequestBody.create(MediaType.parse(contentType), value));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        //POST harus ada body
        RequestBody requestBody = multipartBuilder.build();
        requestBuilder.post(requestBody);

        return requestBuilder.build();
    }


    /**
     * MULTIPART REQUEST DENGAN TIPE BITSTREAM
     **/
    public Request buildMultipartRequestStream(String urls) {

        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(urls);


        //tambahkan header
        int panjangheader = headersmap.size();
        if (panjangheader > 0) {

            for (String key : headersmap.keySet()) {

                String value = headersmap.get(key);
                requestBuilder.addHeader(key, value);
            }
        }


        //tambahkan body string
        int panjangbodystring = bodystrings.size();
        if (panjangbodystring > 0) {

            for (String key : bodystrings.keySet()) {

                String value = bodystrings.get(key);
                multipartBuilder.addFormDataPart(key, value);

            }
        }

        //masukkan file ke dalam body
        int panjangbodyfiles = bodyfiles.size();
        if (panjangbodyfiles > 0) {

            byte[] bytedatas;

            for (String key : bodyfiles.keySet()) {

                String alamatfiles = bodyfiles.get(key);

                try {
                    File value = new File(alamatfiles);
                    String name = value.getName();

                    bytedatas = konversiKeByteStreamOkio(alamatfiles);


                    multipartBuilder.addFormDataPart(key, name,
                            RequestBody.create(MediaType.parse("application/octet-stream"), bytedatas));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        //POST harus ada body
        RequestBody requestBody = multipartBuilder.build();
        requestBuilder.post(requestBody);

        return requestBuilder.build();
    }


    /**
     * KONVERSI FILE KE BIT STREAM
     **/
    public byte[] konversiKeByteStreamOkio(String alamatgambar) {

        byte[] bytes = null;
        byte[] byteselesai = new byte[8196];

        //kompres gambar
        Bitmap gambarasli;

        try {

            InputStream inputstreamkonv = new FileInputStream(alamatgambar);
            BufferedSource bufferedSource = Okio.buffer(Okio.source(inputstreamkonv));
            bytes = bufferedSource.readByteArray();
            bufferedSource.close();

            //kompres gambar
            ByteArrayOutputStream outputblob = new ByteArrayOutputStream();
            gambarasli = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            gambarasli.compress(Bitmap.CompressFormat.JPEG, 80, outputblob);

            BufferedSink bufferedSink = Okio.buffer(Okio.sink(outputblob));
            bufferedSink.write(byteselesai);

        } catch (Exception e) {
            e.printStackTrace();
            byteselesai = null;
        }


        return byteselesai;
    }


}